package com.energy.task.service;

import com.energy.task.BaseTest;
import com.energy.task.model.Month;
import com.energy.task.model.Readings;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;

import static org.mockito.BDDMockito.*;

/**
 * Unit test for {@link ConsumptionService}
 */
public class ConsumptionServiceTest extends BaseTest {

	@Autowired
	@Qualifier("consumptionService")
	ConsumptionService consumptionService;

	@MockBean
	@Qualifier("readingsService")
	ReadingsService readingsService;

	@Test
	public void testConsumption(){
		Readings readings = new Readings("0001", "A", Arrays.asList(30, 70));
		when(readingsService.find(eq(readings.getConnectionId()))).thenReturn(readings);
		Assert.assertEquals(Integer.valueOf(30),consumptionService.calcConsumption(readings.getConnectionId(), Month.JAN.name()));
		Assert.assertEquals(Integer.valueOf(40),consumptionService.calcConsumption(readings.getConnectionId(), Month.FEB.name()));
	}
}
