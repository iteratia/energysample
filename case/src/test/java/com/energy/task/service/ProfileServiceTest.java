package com.energy.task.service;

import com.energy.task.BaseTest;
import com.energy.task.dao.ProfileRepository;
import com.energy.task.model.Profile;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;

import static org.mockito.Matchers.same;

/**
 * Unit test for {@link ProfileService}
 */
public class ProfileServiceTest extends BaseTest {

	private final Profile INVALID_PROFILE = new Profile("A", Arrays.asList(
			0.1, 0.1, 0.1, 0.05, 0.05, 0.05, 0.05, 0.1, 0.1, 0.1, 0.1, 0.1, 0.5 // invalid sum
	));

	private final Profile CORRECT_PROFILE = new Profile("A", Arrays.asList(
			0.01, 0.1, 0.1, 0.05, 0.05, 0.05, 0.05, 0.1, 0.1, 0.1, 0.1, 0.19
	));

	@Autowired
	@Qualifier("profileService")
	ProfileService profileService;

	@MockBean(name = "profileRepository")
	ProfileRepository profileRepository;


	@Test
	public void testSuccess() {
		profileService.save(CORRECT_PROFILE);
		BDDMockito.verify(profileRepository).save(same(CORRECT_PROFILE));
	}

	@Test(expected = DataValidationException.class)
	public void testInvalidSum() {
		profileService.save(INVALID_PROFILE);
	}

}
