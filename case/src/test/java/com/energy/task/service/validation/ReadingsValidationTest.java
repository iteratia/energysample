package com.energy.task.service.validation;

import com.energy.task.BaseTest;
import com.energy.task.model.Profile;
import com.energy.task.model.Readings;
import com.energy.task.service.ProfileService;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;

import static org.mockito.BDDMockito.eq;
import static org.mockito.BDDMockito.when;

/**
 * Unit test for {@link ReadingValidator}
 */
public class ReadingsValidationTest extends BaseTest {

	private static final String A = "A";
	private static final String ID = "0001";

	@MockBean(name = "profileService")
	private ProfileService profileService;

	@Autowired
	@Qualifier("readingValidator")
	private ReadingValidator readingValidator;

	@Test
	public void testSuccessCase() {
		when(profileService.find(eq(A))).thenReturn(new Profile(A, Arrays.asList(0.3, 0.3, 0.2, 0.2)));
		ValidationResult result = readingValidator.validate(new Readings(ID, A, Arrays.asList(30, 60, 80, 100)));
		Assert.assertTrue("Readings should be valid", result.isValid());
	}

	@Test
	public void testMonthConsumption() {
		when(profileService.find(eq(A))).thenReturn(new Profile(A, Arrays.asList(0.99)));
		ValidationResult result = readingValidator.validate(new Readings(ID, A, Arrays.asList(100, 99)));
		Assert.assertEquals(ValidationResult.ValidationError.INVALID_READING, result.getError());
	}

	@Test
	public void testProfileExists() {
		when(profileService.find(eq(A))).thenReturn(null);
		ValidationResult result = readingValidator.validate(new Readings(ID, A, Arrays.asList(10)));
		Assert.assertEquals(ValidationResult.ValidationError.PROFILE_NOT_FOUND, result.getError());
	}

	@Test
	public void testExpectedConsumption() {
		when(profileService.find(eq(A))).thenReturn(new Profile(A, Arrays.asList(0.3, 0.4, 0.3)));
		ValidationResult result = readingValidator.validate(new Readings(ID, A, Arrays.asList(40, 60, 100)));
		Assert.assertEquals(ValidationResult.ValidationError.INVALID_CONSUMPTION, result.getError());
	}
}
