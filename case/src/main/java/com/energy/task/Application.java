package com.energy.task;

import com.energy.task.model.Profile;
import com.energy.task.model.Readings;
import com.energy.task.transport.ProfileTO;
import com.energy.task.transport.ReadingsTO;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.GenericConverter;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import static springfox.documentation.builders.PathSelectors.regex;

/**
 * Application runner
 */
@SpringBootApplication
@EnableSwagger2
@EnableScheduling
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.any())
				.paths(regex("/api.*"))
				.build();
	}

	@Bean
	public DataSource dataSource() {
		return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.HSQL).build();
	}

	@Bean(name = "conversionService")
	public ConversionService getConversionService() {
		DefaultConversionService service = new DefaultConversionService();
		service.addConverter(getProfileEntryConverter());
		service.addConverter(getMetricConverter());
		return service;
	}


	private GenericConverter getMetricConverter() {
		return new GenericConverter() {
			@Override
			public Set<ConvertiblePair> getConvertibleTypes() {
				return Arrays
						.stream(new ConvertiblePair[]{
								new ConvertiblePair(ReadingsTO.class, Readings.class),
								new ConvertiblePair(Readings.class, ReadingsTO.class)})
						.collect(Collectors.toSet());
			}

			@Override
			public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
				if (sourceType.getObjectType().equals(ReadingsTO.class)) {
					ReadingsTO readingsTo = (ReadingsTO) source;
					return new Readings(readingsTo.getConnectionId(), readingsTo.getProfile(), readingsTo.getValues());
				}
				if (sourceType.getObjectType().equals(Readings.class)) {
					Readings readings = (Readings) source;
					return new ReadingsTO(readings.getConnectionId(), readings.getProfile(), readings.getValues());
				}
				return new IllegalArgumentException();
			}
		};
	}

	private GenericConverter getProfileEntryConverter() {
		return new GenericConverter() {
			@Override
			public Set<ConvertiblePair> getConvertibleTypes() {
				return Arrays
						.stream(new ConvertiblePair[]{
								new ConvertiblePair(ProfileTO.class, Profile.class),
								new ConvertiblePair(Profile.class, ProfileTO.class)})
						.collect(Collectors.toSet());
			}

			@Override
			public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
				if (sourceType.getObjectType().equals(ProfileTO.class)) {
					ProfileTO entryTO = (ProfileTO) source;
					return new Profile(entryTO.getProfile(), entryTO.getFractions());
				}
				if (sourceType.getObjectType().equals(Profile.class)) {
					Profile entry = (Profile) source;
					return new ProfileTO(entry.getProfile(), entry.getFractions());
				}
				return new IllegalArgumentException();
			}
		};
	}
}
