package com.energy.task.service;

/**
 * Service to calculate consumption
 */
public interface ConsumptionService {

	/**
	 * Calculates consumption value for specified connection id and month
	 *
	 * @param connectionId connection id
	 * @param month        month
	 * @return consumption value, null if no data for connection id
	 */
	Integer calcConsumption(String connectionId, String month);
}
