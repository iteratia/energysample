package com.energy.task.service;

import com.energy.task.model.Profile;

/**
 * Service for operations with {@link Profile}
 */
public interface ProfileService {

	/**
	 * Validates and saves profile
	 *
	 * @param profile profile
	 * @throws DataValidationException if profile is invalid
	 */
	void save(Profile profile) throws DataValidationException;

	/**
	 * Find profile by name
	 *
	 * @param profileName name
	 * @return profile
	 */
	Profile find(String profileName);

	/**
	 * Deletes profile by name
	 *
	 * @param profileName name
	 */
	void delete(String profileName);
}
