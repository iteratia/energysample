package com.energy.task.transport;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * Model to transfer a metric readings using REST api
 */
@ApiModel(description = "Metric readings")
public class ReadingsTO {

	@ApiModelProperty(value = "The Connection ID", required = true, example = "0001")
	private String connectionId;

	@ApiModelProperty(value = "The profile name", required = true, example = "A")
	private String profile;

	@ApiModelProperty(value = "The collection of metric readings,ex [50,100]", required = true)
	private List<Integer> values;

	public ReadingsTO() {
	}


	public ReadingsTO(String connectionId, String profile, List<Integer> values) {
		this.connectionId = connectionId;
		this.profile = profile;
		this.values = values;
	}

	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public List<Integer> getValues() {
		return values;
	}

	public void setValues(List<Integer> values) {
		this.values = values;
	}

	@Override
	public String toString() {
		return "ReadingsTO{" +
				"connectionId='" + connectionId + '\'' +
				", profile='" + profile + '\'' +
				", values=" + values +
				'}';
	}
}
