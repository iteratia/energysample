package com.energy.task.controller;

import com.energy.task.service.ConsumptionService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller to retrieve consumption
 */
@RestController("consumptionController")
@RequestMapping("api/consumption")
@Api(value = "consumption", description = "Consumption information")
public class ConsumptionController {

	@Autowired
	@Qualifier("consumptionService")
	private ConsumptionService consumptionService;

	@ApiOperation(value = "Retrieve consumption by connectionId and month")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Consumption successfully calculated"),
			@ApiResponse(code = 404, message = "No data for specified connection id"),
	}
	)
	@RequestMapping(value = "/get/{connectionId}/{month}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	ResponseEntity<Integer> get(@PathVariable("connectionId") String connectionId, @PathVariable("month") String month) {
		Integer consumption = consumptionService.calcConsumption(connectionId, month);
		if (consumption == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(consumption, HttpStatus.OK);
		}
	}

}
