package com.energy.task.dao;

import com.energy.task.model.Readings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * DAO for {@link Readings} entity
 */
@Repository("readingsRepository")
@Transactional
public interface ReadingsRepository extends JpaRepository<Readings, String> {
}
