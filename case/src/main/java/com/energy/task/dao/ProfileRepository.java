package com.energy.task.dao;

import com.energy.task.model.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * DAO for {@link Profile} entity
 */
@Repository("profileRepository")
@Transactional
public interface ProfileRepository extends JpaRepository<Profile, String> {

}
