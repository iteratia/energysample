package com.energy.task.model;

import javax.persistence.*;
import java.util.List;

/**
 * Meter readings entity
 */
@Entity
@Table(name = "readings")
public class Readings {


	/**
	 * Connection Id
	 */
	@Id
	private String connectionId;

	/**
	 * Profile name
	 */
	@Column
	private String profile;

	/**
	 * Collection of reading values for each month from JAN to DEC
	 */
	@Column(name = "reading_list")
	@ElementCollection(targetClass = Integer.class)
	private List<Integer> values;

	public Readings() {
	}


	public Readings(String connectionId, String profile, List<Integer> values) {
		this.connectionId = connectionId;
		this.profile = profile;
		this.values = values;
	}


	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public List<Integer> getValues() {
		return values;
	}

	public void setValues(List<Integer> values) {
		this.values = values;
	}

	@Override
	public String toString() {
		return "Readings{" +
				"connectionId='" + connectionId + '\'' +
				", profile='" + profile + '\'' +
				", values=" + values +
				'}';
	}
}
