package com.energy.task.model;

import org.springframework.util.Assert;

/**
 * 3-symbol month presentation
 */
public enum Month implements Comparable<Month> {
	JAN(0),
	FEB(1),
	MAR(2),
	APR(3),
	MAY(4),
	JUN(5),
	JUL(6),
	AUG(7),
	SEP(8),
	OCT(9),
	NOV(10),
	DEC(11);

	/**
	 * Order in year from 0 to 11
	 */
	private int order;

	Month(int order) {
		this.order = order;
	}

	public static int compare(String month1, String month2) {
		Assert.notNull(month1, "Argument 'month1' can't be null");
		Assert.notNull(month2, "Argument 'month2' can't be null");
		return Month.valueOf(month1).compareTo(Month.valueOf(month2));
	}

	public int getOrder() {
		return order;
	}

}
