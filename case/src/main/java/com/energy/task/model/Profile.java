package com.energy.task.model;

import javax.persistence.*;
import java.util.List;

/**
 * The profile entity
 */
@Entity
@Table(name = "profile")
public class Profile {

	/**
	 * Profile name
	 */
	@Id
	private String profile;

	/**
	 * Collection of fractions for each month
	 */
	@Column(name = "fractions")
	@ElementCollection(targetClass = double.class)
	private List<Double> fractions;

	public Profile() {

	}

	public Profile(String profile, List<Double> fractions) {
		this.profile = profile;
		this.fractions = fractions;
	}


	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public List<Double> getFractions() {
		return fractions;
	}

	public void setFractions(List<Double> fractions) {
		this.fractions = fractions;
	}

	@Override
	public String toString() {
		return "Profile{" +
				"profile='" + profile + '\'' +
				", fractions=" + fractions +
				'}';
	}
}
